package Games.Nim.Players;
import Games.Nim.Moves.Move;
import Games.Nim.Moves.MoveToken;
import Games.Nim.IBoard;
import Board.Path.PathCoordinate;
import Core.NameAvatar;
/**
 * This is the AI asked for the preproject in bonus. It play just before BonusAI2 and make a cooperative decision to win against a thrid player.
 *
 * @author Bury Jason
 */
public class BonusAI1 extends RandomAI {

	private int m;//The maximum leap allowed +1

	public BonusAI1(NameAvatar avatar) {
		super(avatar);
	}

	@Override
	public void informMaxLeap(int maxleap){
		this.m = maxleap+1;
		super.informMaxLeap(maxleap);
	}

	@Override
	public Move pickMove() {
		//first, find our position
		int position = getBoard().getTokenPosition().getI();
		//then compute if the other player will win if he play correctly
		int reste = position%m;
		if(reste==1 && getMaxLeap()<2){//so the other player will win if he play correctly
			return super.pickMove();
		}
		else{//so make the right decision and bonusAI2 or you will win
			if(reste<=1){
				return new MoveToken(Math.min(getMaxLeap(), position));
			}
			else{
				return new MoveToken(Math.min(getMaxLeap(),reste-1));
			}
		}
	}
}
