package Games.Nim.Players;
import Games.Nim.Moves.Move;
import Games.Nim.Moves.MoveToken;
import Games.Nim.IBoard;
import Board.Path.PathCoordinate;
import Core.NameAvatar;
/**
 * This is the AI asked for the preproject. It has to make an optimized decision when its sure to win if it play corectly else it play randomly.
 * The implementation of a random decision comes from RandomAI
 *
 * @author Bury Jason and Deleersnyder Kevin
 */
public class BuryDeleersnyderAI extends RandomAI {

	private int m;//The maximum leap allowed +1

	public BuryDeleersnyderAI(NameAvatar avatar) {
		super(avatar);
	}

	@Override
	public void informMaxLeap(int maxleap){
		this.m = maxleap+1;
		super.informMaxLeap(maxleap);
	}

	@Override
	public Move pickMove() {
		//first, find our position
		int position = getBoard().getTokenPosition().getI();
		//then compute if the other player will win if he play correctly
		int reste = position%m;
		if(reste == 0){//so the other player will win if he play correctly
			return super.pickMove();
		}
		else{//so make the right decision and you will win
			return new MoveToken(reste);
		}
	}
}
