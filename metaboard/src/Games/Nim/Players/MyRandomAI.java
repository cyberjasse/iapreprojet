package Games.Nim.Players;

import Core.NameAvatar;
import Games.Nim.Player;
import Games.Nim.Moves.Move;
import Games.Nim.Moves.MoveToken;

/**
 * This random AI plays randomly while its position is greater than maxleap. Else it moves to win.
 * @author Bury Jason
 */
public class MyRandomAI extends RandomAI{
	
	public MyRandomAI(NameAvatar avatar) {
		super(avatar);
	}
	
	@Override
	public Move pickMove() {
		//first, find our position
		int position = getBoard().getTokenPosition().getI();
		//then compute if we can win right now
		if(position <= getMaxLeap()){//so we are going to win
			return new MoveToken(position);
		}
		else{//play random
			return super.pickMove();
		}
	}
}
